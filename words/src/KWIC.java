import java.util.*;


public class KWIC {


    public static void main(String[] args) {
        ArrayList<ArrayList<String>> original = new ArrayList();
        ArrayList<modified> sortlist = new ArrayList();
        String [] illegal={"is","the","of","and","as","a","but"};
        ArrayList<String> first = new ArrayList();
        ArrayList<String> second = new ArrayList();
        ArrayList<String> third = new ArrayList();
        ArrayList<String> fourth = new ArrayList();
        ArrayList<String> fifth = new ArrayList();
        first.add(("Descent of Man").toLowerCase());
        second.add(("The Ascent of Man").toLowerCase());
        third.add(("The Old Man and The Sea").toLowerCase());
        fourth .add(("A Portrait of The Artist As a Young Man").toLowerCase());
        fifth.add(("A Man is a Man but Bubblesort IS A DOG").toLowerCase());
        original.add(first);
        original.add(second);
        original.add(third);
        original.add(fourth);
        original.add(fifth);
        for(int i=0;i<5;i++){
            String[] words = original.get(i).get(0).split(" ");
            for(int j=0;j<words.length;j++){
                if( !(Arrays.asList(illegal).contains( words[j] ) )){
                    sortlist.add( new modified( original.get(i).get(0), words[j]) );
                }
            }
        }

        if (sortlist.size() > 0) {
            Collections.sort(sortlist, new Comparator<modified>() {
                @Override
                public int compare(final modified object1, final modified object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }

        for(int k=0; k< sortlist.size();k++){
            System.out.println( sortlist.get(k).name );
        }

    }

}
